# SPDX-FileCopyrightText: 2022 Jonah Brüchert <jbb@kaidan.im>
#
# SPDX-License-Identifier: EUPL-1.2

PREFIX := /usr/local/
CUPS_PREFIX := /usr/
DESTDIR := /
SOURCE_DIRS := gdrascii_codec/ erika_3004/ utf8togdrascii/ erika_cli/ cups/
INSTALL := install
STRIP := strip
INSTALL_EXECUTABLE := $(INSTALL) -s --strip-program=$(STRIP)
TARGET := $(shell rustc -vV | grep host | sed 's/host: //g' | xargs)
OUT_DIR = target/$(TARGET)/release

all: $(OUT_DIR)/utf8togdrascii $(OUT_DIR)/erika-cli

$(OUT_DIR)/erika-cli: $(SOURCE_DIRS)
	cargo build --release --target $(TARGET) --bin erika-cli

$(OUT_DIR)/utf8togdrascii: $(SOURCE_DIRS)
	cargo build --release --target $(TARGET) --bin utf8togdrascii

clean:
	rm $(OUT_DIR) -rf

install-filters:
	mkdir -p $(DESTDIR)/$(CUPS_PREFIX)/lib/cups/filter/
	$(INSTALL) -m755 cups/filters/pdftogdrascii     $(DESTDIR)/$(CUPS_PREFIX)/lib/cups/filter/
	$(INSTALL) -m755 cups/filters/utf8togdrascii    $(DESTDIR)/$(CUPS_PREFIX)/lib/cups/filter/
	sed -i "s,@PREFIX@,${PREFIX},g" $(DESTDIR)/$(CUPS_PREFIX)/lib/cups/filter/pdftogdrascii $(DESTDIR)/$(CUPS_PREFIX)/lib/cups/filter/utf8togdrascii

install: $(OUT_DIR)/erika-cli $(OUT_DIR)/utf8togdrascii install-filters
	mkdir -p $(DESTDIR)/$(PREFIX)/bin/
	$(INSTALL_EXECUTABLE) -m755 $(OUT_DIR)/erika-cli       $(DESTDIR)/$(PREFIX)/bin/
	$(INSTALL_EXECUTABLE) -m755 $(OUT_DIR)/utf8togdrascii  $(DESTDIR)/$(PREFIX)/bin/

test:
	cargo test
