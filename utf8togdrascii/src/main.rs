// SPDX-FileCopyrightText: 2022 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: EUPL-1.2

use std::io::{self, Read, Write};

fn main() -> io::Result<()> {
    let stdin = io::stdin();
    let mut stdin_handle = stdin.lock();

    let stdout = io::stdout();
    let mut stdout_handle = stdout.lock();

    let mut buf = [0u8; 512];

    let mut size = stdin_handle.read(&mut buf)?;
    while size > 0 {
        let input = std::str::from_utf8(&buf[..size]).unwrap();
        let encoded = gdrascii_codec::encode(input);
        stdout_handle.write_all(&encoded)?;
        size = stdin_handle.read(&mut buf)?;
    }

    stdout_handle.flush()?;
    Ok(())
}
